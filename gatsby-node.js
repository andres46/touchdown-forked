/* eslint-disable */

require("dotenv").config({ path: `.env.${process.env.NODE_ENV}` })
const path = require("path")

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  const remarkFilesResult = await graphql(`
    {
      allMarkdownRemark {
        nodes {
          id
          parent {
            ... on File {
              name
              relativeDirectory
            }
          }
          html
          frontmatter {
            title
          }
        }
      }
    }
  `)

  remarkFilesResult.data.allMarkdownRemark.nodes.forEach((node) => {
    createPage({
      path:
        node?.parent?.name === "README" &&
        node?.parent?.relativeDirectory === ""
          ? "/"
          : `${node?.parent?.relativeDirectory}/${node?.parent?.name}.md`,
      component: path.resolve("./src/components/template.tsx"),
      context: {
        title: node?.frontmatter?.title,
        html: node.html,
      },
    })
  })
}
