import { Issue } from "../domain/interfaces"

export function getIssuesList(projectId: string): Promise<void | Array<Issue>> {
  return fetch(`https://gitlab.com/api/v4/projects/${projectId}/issues`)
    .then((data) => data.json())
    .catch((err) => {
      console.error(err)
    })
}
