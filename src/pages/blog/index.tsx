import React from "react"
import { Navbar } from "../../components/Navbar/Navbar"
import { graphql } from "gatsby"
import { Link } from "@reach/router"

interface OverviewProps {
  data: {
    allFile: {
      nodes: any[]
    }
  }
}

const Overview = (props: OverviewProps) => {
  const {
    data: {
      allFile: { nodes },
    },
  } = props

  return (
    <div>
      <Navbar />
      <main>
        <h1>Blogs list</h1>
        <div>
          {nodes?.length > 0 ? (
            nodes?.map(({ name }: { name: string }) => (
              <div key={`blog-${name}`}>
                <Link to={`/blog/${name}.md/`}>{name}</Link>
              </div>
            ))
          ) : (
            <div>
              <p>No blogs to render</p>
            </div>
          )}
        </div>
      </main>
    </div>
  )
}

export default Overview

export const blogsQuery = graphql`
  {
    allFile(filter: { relativeDirectory: { eq: "blog" } }) {
      nodes {
        name
      }
    }
  }
`
