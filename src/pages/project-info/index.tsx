import React from "react"
import { Navbar } from "../../components/Navbar/Navbar"
import "./project-info.scss"
import dayjs from "dayjs"
import { useProjectInfo } from "../../custom-hooks/useProjectInfo"
import { useIssuesInfo } from "../../custom-hooks/useIssuesInfo"

const Index = () => {
  const projectQuery = useProjectInfo()
  const issuesQuery = useIssuesInfo()
  const { isLoading, isSuccess, data } = projectQuery
  const {
    isLoading: isLoadingIssues,
    isSuccess: isSuccessIssues,
    data: dataIssues,
  } = issuesQuery

  return (
    <div>
      <Navbar />
      <main className="project-info">
        {isLoading ? (
          <p>Loading...</p>
        ) : (
          isSuccess && (
            <>
              <h3>Name: {data?.name}</h3>
              <h3>Description:</h3>
              <p className="description">{data?.description}</p>
              <h3>Forks count: {data?.forks_count}</h3>
              {data?.topics && data?.topics?.length > 0 && (
                <div>
                  <h3>Topics:</h3>
                  {data?.topics.map((topic) => (
                    <p key={`topics-${topic}`}>{topic}</p>
                  ))}
                </div>
              )}
              {isLoadingIssues ? (
                <p>loading issues...</p>
              ) : (
                isSuccessIssues && (
                  <div className="issues-list">
                    <h3>Issues:</h3>
                    {dataIssues?.map((issue, index) => (
                      <div
                        key={`issue-${issue.title}-${index}`}
                        className="issues-list-item"
                        style={index === 0 ? { border: "none" } : {}}
                      >
                        <div className="title-div flex">
                          <p className="subtitle">Title:</p>
                          <p>{issue.title}</p>
                        </div>
                        <div className="title-div flex">
                          <p className="subtitle">Created at:</p>
                          <p>{dayjs(issue.created_at).format("DD.MM.YYYY")}</p>
                        </div>
                        <div className="title-div flex">
                          <p className="subtitle">State:</p>
                          <p
                            style={{
                              color: issue.state === "0" ? "red" : "green",
                              fontWeight: "700",
                            }}
                          >
                            {issue.state === "0" ? "closed" : "open"}
                          </p>
                        </div>
                        <div className="title-div flex">
                          <p className="subtitle">Author:</p>
                          <p>{issue.author.name}</p>
                        </div>
                        <p className="subtitle">Description: </p>
                        <p className="">{issue.description}</p>
                      </div>
                    ))}
                  </div>
                )
              )}
            </>
          )
        )}
      </main>
    </div>
  )
}

export default Index
