interface GitlabUser {
  state: string
  web_url: string
  avatar_url?: string
  username: string
  id: number
  name: string
}

type State = "0" | "1"

interface Milestone {
  due_date: string
  project_id: string
  state: State
  description: string
  iid: string
  id: string
  title: string
  created_at: string
  updated_at: string
}

export interface ProjectData {
  name: string
  description: null | string
  avatar_url: string
  forks_count: number
  topics: string[]
}

export interface Issue {
  milestone: Milestone
  author: GitlabUser
  description: string
  assignees: Array<GitlabUser>
  type: string
  labels: Array<string>
  title: string
  created_at: string
  state: State
}
