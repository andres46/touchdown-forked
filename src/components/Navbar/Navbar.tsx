import React, { useState } from "react"
import { Link, graphql, useStaticQuery } from "gatsby"
import "./Navbar.scss"
import { useProjectInfo } from "../../custom-hooks/useProjectInfo"
import { GiHamburgerMenu } from "react-icons/gi"
import { useLocation } from "@reach/router"

const calculateOrigin = (
  isGitlabOrigin: boolean,
  hostname: string,
  protocol: string,
  pathname: string
) => {
  const pathnameParts = pathname.split("/")
  // pathname is the content of the url after the host, so pathname could be for example:
  // '/touchdown/contribute.md' for Gitlab pages,
  // hence splitting this url into parts will return [ "", "touchdown", "contribute.md" ]
  return isGitlabOrigin ? `${protocol}//${hostname}/${pathnameParts[1]}` : "/"
}

export const Navbar = () => {
  const location = useLocation()
  const isGitlabOrigin = location?.href?.includes("gitlab.io")
  const projectQuery = useProjectInfo()
  const { isLoading, isSuccess, data } = projectQuery
  const [showOptionsMobile, setShowOptionsMobile] = useState(false)

  const query = useStaticQuery(graphql`
    {
      allFile {
        nodes {
          name
          relativeDirectory
        }
      }
    }
  `)

  const options = (
    <ol>
      <li>
        <Link
          to={calculateOrigin(
            isGitlabOrigin,
            location?.hostname,
            location?.protocol,
            location?.pathname
          )}
        >
          {process.env.GATSBY_PROJECT_NAME || "Home"}
        </Link>
      </li>
      <li>
        {query?.allFile?.nodes.filter(
          ({ relativeDirectory }: { relativeDirectory: string }) =>
            relativeDirectory === "blog"
        )?.length > 0 && <Link to="/blog">Blog</Link>}
      </li>
      <li>
        {query?.allFile?.nodes.filter(
          ({ relativeDirectory }: { relativeDirectory: string }) =>
            relativeDirectory === "contribute"
        )?.length > 0 && <Link to="/contribute.md">contribute</Link>}
      </li>
      {/*  
     to be enabled soon
     <li>
        {query?.allFile?.nodes.filter(
          ({ relativeDirectory }: { relativeDirectory: string }) =>
            relativeDirectory === "docs"
        )?.length > 0 && <Link to="/docs">Docs</Link>}
      </li> */}
    </ol>
  )

  return (
    <header className="header">
      <div className="header-content">
        <div className="header-content-link">
          {isLoading ? (
            <div className="avatar" style={{ backgroundColor: "gray" }} />
          ) : (
            isSuccess && (
              <Link to="/">
                <img className="avatar" src={data?.avatar_url} alt="" />
              </Link>
            )
          )}
        </div>
        <div className="header-content-desktop-options">{options}</div>
        <div className="header-content-mobile-options">
          <button
            className={`icon-btn ${showOptionsMobile ? "active" : ""}`}
            onClick={() => setShowOptionsMobile(!showOptionsMobile)}
          >
            <GiHamburgerMenu className="icon" />
          </button>
          {showOptionsMobile && (
            <div className="header-content-mobile-options-list">{options}</div>
          )}
        </div>
      </div>
    </header>
  )
}
