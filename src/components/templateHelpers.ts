export const preprocessHtml = (html: string) => {
  const result = [...html.matchAll(new RegExp(/<a href="(.*)">.*<\/a>/g))]
  let newHtml = html
  result.forEach((matches) => {
    const linkToOverwrite = matches[1]
    newHtml =
      linkToOverwrite.includes(".md") && !linkToOverwrite.includes("https:")
        ? newHtml.replaceAll(linkToOverwrite, `/${linkToOverwrite}`)
        : newHtml
  })

  return newHtml
}
