import { preprocessHtml } from "./templateHelpers"

test("hello world test", async () => {
  expect(
    preprocessHtml(`
    <main>
      <h1>Documentation</h1>
      <p><a href="contribute.md">contribute</a></p>
      <p><a href="newFile.md">test</a></p>
      <p><a href="https://www.wordreference.com/">Link</a></p>
    </main>
  `)
  ).toBe(`
    <main>
      <h1>Documentation</h1>
      <p><a href="/contribute.md">contribute</a></p>
      <p><a href="/newFile.md">test</a></p>
      <p><a href="https://www.wordreference.com/">Link</a></p>
    </main>
  `)
})
