import React from "react"
import "./Footer.scss"

export const Footer = () => {
  const isFooterVisible = process.env.GATSBY_FOOTER_VISIBLE === "true"

  if (!isFooterVisible) {
    return null
  }

  return (
    <div className="footer">
      <a href="https://touchdown.md/">powered by touchdown.md</a>
    </div>
  )
}
