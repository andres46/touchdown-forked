import * as React from "react"
import { Helmet } from "react-helmet"
import "../styles/styles.scss"
import { Navbar } from "./Navbar/Navbar"
import { preprocessHtml } from "./templateHelpers"
import "./template.scss"
import { Footer } from "./Footer/Footer"

interface PageContext {
  html: string
  title: string
}

interface TemplateProps {
  pageContext: PageContext
}

export default function Template(props: TemplateProps) {
  const { pageContext } = props

  const newHtml = preprocessHtml(pageContext.html)
  return (
    <div className="template">
      <Helmet title={pageContext.title} />
      <Navbar />
      <main className="main" dangerouslySetInnerHTML={{ __html: newHtml }} />
      <Footer />
    </div>
  )
}
