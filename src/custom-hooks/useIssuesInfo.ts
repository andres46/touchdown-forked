import { useQuery } from "react-query"
import { getIssuesList } from "../API/issuesApi"

const projectId = process.env.GATSBY_PROJECT_ID
export const useIssuesInfo = () => {
  const issuesQuery = useQuery(
    ["issues-info", projectId],
    () => {
      if (!projectId) {
        return Promise.reject({})
      }
      return getIssuesList(projectId)
    },
    {
      staleTime: 1000 * 60 * 60,
    }
  )

  return issuesQuery
}
