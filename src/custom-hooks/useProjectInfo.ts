import { useQuery } from "react-query"
import { getProjectInfo } from "../API/projectApi"

export const useProjectInfo = () => {
  const projectId = process.env.GATSBY_PROJECT_ID
  const projectQuery = useQuery(
    ["project-info"],
    () => {
      if (!projectId) {
        return Promise.reject({})
      }
      return getProjectInfo(projectId)
    },
    {
      staleTime: 1000 * 60 * 60,
    }
  )

  return projectQuery
}
