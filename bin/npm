#!/bin/sh
# This shell script provides a predictable docker-based version of the application in use.
# Thereby creating a predictable environment for development, building, and debugging

IMAGE="node:18.14.0-slim"
COMMAND="npm"


if [ "$CI" = "true" ]; then
  echo "Running inside of CI, defaulting DOCKER_OPTS to '' if not set"
  # If inside a CI default DOCKER_OPTS to "" if not set
  export DOCKER_OPTS="${DOCKER_OPTS:-}"
else
  # Outside of CI; We assume to run in a terminal and thus
  # defaulting DOCKER_OPTS to '-it' if not set
  # Bash parameter expansion will only replace _null_ with --interactive --tty
  # the first "-" is for configuring the expansion mode
  export DOCKER_OPTS="${DOCKER_OPTS:--it}"
fi;

# --user and --group-add options simulate the current user inside the docker container
# this is necessary because otherwise the npm command would run as root inside docker
# and the 'node_modules' folder on the host would then belong to host:$(pwd)/root:root
if [ "$(uname -s)" != "Darwin" ] && [ "$(expr substr "$(uname -s)" 1 5)" = "Linux" ]; then
  LINUX_OPTS="--user $(id -u)"
fi

# --platform linux/amd64 is for compatibility with Apple's M1/M2
# --network bridge allows for easier access to the development version from other devices
# --publish 8000:8000 for gatsby applications
# --publish 4200:4200 for angular applications
# --publish 3000:3000 for react applications
# If you server uses different ports you can publish them as needed
exec docker run --rm ${LINUX_OPTS:-} ${DOCKER_OPTS:-} \
  --platform linux/amd64                              \
  --network bridge                                    \
  --volume  "$(pwd)":"$(pwd)"                         \
  --workdir "$(pwd)"                                  \
  --publish 9000:9000                                 \
  --publish 8000:8000                                 \
  --publish 4200:4200                                 \
  --publish 3000:3000                                 \
  "$IMAGE" "$COMMAND" "$@"
