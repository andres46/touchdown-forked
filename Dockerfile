# Use latest "Long Term Support" version
# see also: /.gitlab/.gitlab-build-docker-image.yml
FROM node:18.14.0-slim

WORKDIR /touchdown

COPY . /touchdown

RUN npm install
RUN npm run test

# Run a test build to make sure our shipped code and content builds properly
RUN ./node_modules/.bin/gatsby build
