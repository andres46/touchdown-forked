/* eslint-disable */
const gulp = require("gulp")

async function defaultTask(cb) {
  console.log("----------- Copying the markdown files -----------")
  gulp.src("./*.**").pipe(gulp.dest("./src/content"))
  cb()
}

exports.default = defaultTask
